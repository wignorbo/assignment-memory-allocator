#ifndef TEST_H
#define TEST_H

#define _DEFAULT_SOURCE
#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


struct test_result {
    bool success;
    const char *message;
};

struct test {
    const char *name;

    struct test_result (*test_function)(void);
};

bool prepare_test_env();

struct test_result test_malloc();

struct test_result test_free_one_block();

struct test_result test_free_two_blocks();

struct test_result test_grow_heap_extending();

struct test_result test_grow_heap_not_extending();

void test_all();

#endif //TEST_H
